import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectsComponent } from './components/projects/projects.component';
import { UsersComponent } from './components/users/users.component'
import { TeamsComponent } from './components/teams/teams.component';
import { TasksComponent } from './components/tasks/tasks.component'
import { ExitGuard } from './guards/save-edited-guard';

const routes: Routes = [
  { path: 'projects', component: ProjectsComponent, canDeactivate: [ExitGuard] },
  { path: 'tasks', component: TasksComponent, canDeactivate: [ExitGuard] },
  { path: 'teams', component: TeamsComponent, canDeactivate: [ExitGuard] },
  { path: 'users', component: UsersComponent, canDeactivate: [ExitGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [ExitGuard]
})
export class AppRoutingModule { }
