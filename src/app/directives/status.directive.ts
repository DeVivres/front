import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appStatus]'
})
export class StatusDirective implements OnInit{
  @Input() 
  status: string;
  element: ElementRef;
  
  constructor(element: ElementRef) { 
    this.element = element;
  }

  ngOnInit() {
    this.changeColorOfTheTask();
  }

  private changeColorOfTheTask()
  {
    if(this.status === '2')
    {
      this.element.nativeElement.style.backgroundColor='#add8e6'
    }
    else if(this.status === '3')
    {
      this.element.nativeElement.style.backgroundColor='#66ff00'
    }
    else if(this.status === '4')
    {
      this.element.nativeElement.style.backgroundColor='#ff0000'
    }
  }
}
