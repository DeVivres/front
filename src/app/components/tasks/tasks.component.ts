import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/models/task';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ComponentCanDeactivate } from './../../guards/save-edited-guard';
import { Observable } from "rxjs";

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit, ComponentCanDeactivate {

  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder
  ) { 
    this.newEntityForm = this.formBuilder.group({
      name: '',
      description: '',
      createdAt: '',
      finishedAt: '',
      state: '',
      projectId: '',
      performerId: ''
    });
  }

  tasks: Task[] = [];
  newEntityForm: any;
  showCreateForm: boolean = false;
  savedEdited: boolean = false;

  ngOnInit() {
    this.tasks = [{
      id: 1,
      name: 'Task for capital',
      description: 'Learn about capital',
      createdAt: new Date(2011, 11, 8),
      finishedAt: new Date(2009, 12, 4),
      state: 2,
      projectId: 1,
      performerId: 1,
    },
    {
      id: 2,
      name: 'Task for finance',
      description: 'Learn about finances',
      createdAt: new Date(2016, 4, 5),
      finishedAt: new Date(2017, 2, 9),
      state: 3,
      projectId: 2,
      performerId: 2,
    }]
  }
  
  // ngOnInit(): void {
  //   this.http.get(`https://localhost:44313/api/tasks`).subscribe((data: Task[]) => this.tasks = data);
  // }

  onSubmit(data: Task) {   
    this.newEntityForm.reset();

    this.http.post('https://localhost:44313/api/tasks', data).subscribe(
      (resp) => {
          if (resp) {
              window.location.reload();
          }
      },
      (error) => console.error(error));
  }

  public createForm() {
    this.showCreateForm = !this.showCreateForm;
  }
  
  canDeactivate(): boolean | Observable<boolean>{
    if(!this.savedEdited) 
    {
      return confirm("You are leaving the page. Are you sure?");
    }
    else 
    {
      return true;
    }
  }

  public remove(id: number) {
    this.http.delete(`https://localhost:44313/api/tasks/${id}`).subscribe(() => window.location.reload());
  }
}
