import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ComponentCanDeactivate } from './../../guards/save-edited-guard';
import { Observable } from "rxjs";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})

export class UsersComponent implements OnInit, ComponentCanDeactivate {

  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder
  ) { 
    this.newEntityForm = this.formBuilder.group({
      firstName: '',
      lastName: '',
      email: '',
      teamId: '',
      birthday: ''
    });
  }

  users: User[] = [];
  newEntityForm: any;
  showCreateForm: boolean = false;
  savedEdited: boolean = false;

  ngOnInit() {
    this.users = [{
      id: 1,
      firstName: 'Tom',
      lastName: 'Vivre',
      email: 'tomvivre@gmail.com',
      birthday: new Date(1997, 6, 12),
      registeredAt: new Date(2016, 9, 1),
      teamId: 1,
    },
    {
      id: 2,
      firstName: 'David',
      lastName: 'Sank',
      email: 'davidsank@gmail.com',
      birthday: new Date(2000, 3, 6),
      registeredAt: new Date(2016, 5, 2),
      teamId: 2,
    }]
  }
  // ngOnInit(): void {
  //   this.http.get(`https://localhost:44313/api/users`).subscribe((data: User[]) => this.users = data);
  //   }

  onSubmit(data: User) {   
    this.newEntityForm.reset();

    let date = new Date();
    data.registeredAt = new Date(`${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`);

    this.http.post('https://localhost:44313/api/users', data).subscribe(
      (resp) => {
          if (resp) {
              window.location.reload();
          }
      },
      (error) => console.error(error));
  }

  public createForm() {
    this.showCreateForm = !this.showCreateForm;
  }

  canDeactivate(): boolean | Observable<boolean>{
    if(!this.savedEdited) 
    {
      return confirm("You are leaving the page. Are you sure?");
    }
    else 
    {
      return true;
    }
  }

  public remove(id: number) {
    this.http.delete(`https://localhost:44313/api/users/${id}`).subscribe(() => window.location.reload());
  }
}
