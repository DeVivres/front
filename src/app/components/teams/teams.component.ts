import { Component, OnInit } from '@angular/core';
import { Team } from 'src/app/models/team';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ComponentCanDeactivate } from './../../guards/save-edited-guard';
import { Observable } from "rxjs";

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css']
})
export class TeamsComponent implements OnInit, ComponentCanDeactivate {

  constructor( 
    private http: HttpClient,
    private formBuilder: FormBuilder
    ) { 
      this.newEntityForm = this.formBuilder.group({
        id: '',
        name: '',
        createdAt: ''
      });
    }

    teams: Team[] = [];
    newEntityForm: any;
    showCreateForm: boolean = false;
    savedEdited: boolean = false;

  ngOnInit() {
    this.teams = [{ 
      id: 1,
      name: 'Capital Team',
      createdAt: new Date(2011, 11, 8),
    },
    {
      id: 2,
      name: 'Financial Team',
      createdAt: new Date(2016, 4, 5),
    }]
  }
    
  // ngOnInit(): void {
  //   this.http.get(`https://localhost:44313/api/teams`).subscribe((data: Team[]) => this.teams = data);
  //   }
  
  onSubmit(data: Team) {   
    this.newEntityForm.reset();

    this.http.post('http://localhost:44313/api/teams', data).subscribe(
      (resp) => {
          if (resp) {
              window.location.reload();
          }
      },
      (error) => console.error(error));
  }

  public createForm() {
    this.showCreateForm = !this.showCreateForm;
  }

  canDeactivate(): boolean | Observable<boolean>{
    if(!this.savedEdited) 
    {
      return confirm("You are leaving the page. Are you sure?");
    }
    else 
    {
      return true;
    }
  }

  public remove(id: number) {
    this.http.delete(`https://localhost:44313/api/teams/${id}`).subscribe(() => window.location.reload());
  }
}
