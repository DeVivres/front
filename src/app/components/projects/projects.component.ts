import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/models/project';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ComponentCanDeactivate } from './../../guards/save-edited-guard';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit, ComponentCanDeactivate {

  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder
  ) { 
    this.newEntityForm = this.formBuilder.group({
      name: '',
      description: '',
      createdAt: '',
      deadline: '',
      authorId: '',
      teamId: ''
    });
  }

  projects: Project[] = [];
  newEntityForm: any;
  showCreateForm: boolean = false;
  savedEdited: boolean = false;

  ngOnInit() {
    this.projects = [{ 
      id: 1,
      name: 'Capital Project',
      description: 'Project about capital',
      createdAt: new Date(2011, 11, 8),
      deadline: new Date(2009, 12, 4),
      authorId: 1,
      teamId: 1
    },
    {
      id: 2,
      name: 'Finance Project',
      description: 'Project about finances',
      createdAt: new Date(2016, 4, 5),
      deadline: new Date(2017, 2, 9),
      authorId: 2,
      teamId: 2,
    }]
  }
  
  // ngOnInit(): void {
  //   this.http.get(`https://localhost:44313/api/projects`).subscribe((data: Project[]) => this.projects = data);
  // }

  onSubmit(data: Project) {   
    this.newEntityForm.reset();
    this.savedEdited = true;

    this.http.post('http://localhost:44313/api/projects', data).subscribe(
      (resp) => {
          if (resp) {
              window.location.reload();
          }
      },
      (error) => console.error(error));
  }

  public createForm() {
    this.showCreateForm = !this.showCreateForm;
  }

  canDeactivate(): boolean | Observable<boolean>{
    if(!this.savedEdited) 
    {
      return confirm("You are leaving the page. Are you sure?");
    }
    else 
    {
      return true;
    }
  }

  public remove(id: number) {
    this.http.delete(`https://localhost:44313/api/projects/${id}`).subscribe(() => window.location.reload());
  }
}
