export interface TaskState {
    id: number;
    value: string;
}
